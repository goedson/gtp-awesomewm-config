#!/bin/sh
# Simple script for installing configuration files
#
# For each file contained in config-files, creates a symbolic link to it in
# the user's home directory. Subdirectories are created as needed.

# Assume the configuration files to be installed are in the config-files directory at the
# same path where this script is called from.
CONFIG_FILES_SOURCE_PATH=`dirname $0`

#Skip version control directory and some documentations files.
IGNORED_REGEX="\.git.*|\.md|\.png|\.spec|~|#"

stow --target="${HOME}/.config" --ignore="${IGNORED_REGEX}" --dir="${CONFIG_FILES_SOURCE_PATH}" config-files
